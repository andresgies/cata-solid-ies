package com.codurance.lsp;

public class HibrydCar extends Vehicle {

	private static final int FUEL_TANK_FULL = 100;
	private int fuelTankLevel = 0;

	private static final int BATTERY_FULL = 100;
	private int batteryLevel;

	public int batteryLevel() {
		return batteryLevel;
	}

	public int fuelTankLevel() {
		return fuelTankLevel;
	}

	@Override
	public void fullOrChargue() {
		this.fuelTankLevel = FUEL_TANK_FULL;
		this.batteryLevel = BATTERY_FULL;
	}

}
