package com.codurance.lsp;

public class ElectricCar extends Vehicle {

	private static final int BATTERY_FULL = 100;
	private int batteryLevel;

	public int batteryLevel() {
		return batteryLevel;
	}

	@Override
	public void fullOrChargue() {
		batteryLevel = BATTERY_FULL;
	}
}
