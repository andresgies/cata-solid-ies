package com.codurance.lsp;

public class PetrolCar extends Vehicle {
	private static final int FUEL_TANK_FULL = 100;
	private int fuelTankLevel = 0;

	public int fuelTankLevel() {
		return fuelTankLevel;
	}

	@Override
	public void fullOrChargue() {
		this.fuelTankLevel = FUEL_TANK_FULL;

	}
}
