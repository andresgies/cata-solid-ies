package com.codurance.isp;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BuhoBarkingTest {
	private ByteArrayOutputStream consoleContent = new ByteArrayOutputStream();
	private final BuhoBarking buho = new BuhoBarking();

	@BeforeEach
	public void setUp() {
		System.setOut(new PrintStream(consoleContent));
	}

	@Test
	void run() {
		buho.run();
		assertThat(consoleContent.toString()).isEqualTo("Buho is running");
	}

	@Test
	void bark() {
		buho.bark();
		assertThat(consoleContent.toString()).isEqualTo("Buho is barking");
	}

	@Test
	void fly() {
		buho.fly();
		assertThat(consoleContent.toString()).isEqualTo("Buho is flying");
	}

}
