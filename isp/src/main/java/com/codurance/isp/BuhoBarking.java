package com.codurance.isp;

public class BuhoBarking implements AnimalVolador, AnimalLadrador, AnimalTerrestre{

	@Override
	public void bark() {
		System.out.print("Buho is barking");
	}

	@Override
	public void fly() {
		System.out.print("Buho is flying");	
	}

	@Override
	public void run() {
		System.out.print("Buho is running");
	}

}
