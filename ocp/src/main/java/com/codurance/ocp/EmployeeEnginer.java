package com.codurance.ocp;

public class EmployeeEnginer extends Employee {

	EmployeeEnginer(int salary, int bonus, EmployeeType type) {
		super(salary, bonus, type);
	}
	
	@Override
	public int payAmount() {
		return this.salary;
	}

}
