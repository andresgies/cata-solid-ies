package com.codurance.ocp;

public class EmployeeManager extends Employee{

	EmployeeManager(int salary, int bonus, EmployeeType type) {
		super(salary, bonus, type);
	}
	
	@Override
	public int payAmount() {
		return this.salary + this.bonus;
	}

}
