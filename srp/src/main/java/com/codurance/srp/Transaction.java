package com.codurance.srp;

import static java.util.stream.Collectors.toCollection;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Transaction {
    private LocalDate date;
    private int amount;

    Transaction(LocalDate date, int amount) {
        this.date = date;
        this.amount = amount;
    }

    public Transaction() {
		super();
		}

	public LocalDate date() {
        return date;
    }

    public int amount() {
        return amount;
    }
    
    public void printTransactions(List<Transaction> transactions) {
    	Console console = new Console();
    	Format format = new Format();
        final AtomicInteger balance = new AtomicInteger(0);
        transactions.stream()
                .map(transaction -> format.statementLine(transaction, balance.addAndGet(transaction.amount())))
                .collect(toCollection(LinkedList::new))
                .descendingIterator()
                .forEachRemaining(console::printLine);
    }
    
}
