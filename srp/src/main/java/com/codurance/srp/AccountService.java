package com.codurance.srp;

import java.util.List;

public class AccountService {

    private static final String STATEMENT_HEADER = "DATE | AMOUNT | BALANCE";


    private TransactionRepository transactionRepository;
    private Clock clock;
    private Console console;

    public AccountService(TransactionRepository transactionRepository, Clock clock, Console console) {
        this.transactionRepository = transactionRepository;
        this.clock = clock;
        this.console = console;
    }

    public void deposit(int amount) {
        transactionRepository.add(transactionWith(amount));
    }


    public void withdraw(int amount) {
        transactionRepository.add(transactionWith(-amount));
    }

    public void printStatement() {
    	List<Transaction> transactions = transactionRepository.all();
        printHeader();
        printTransactions(transactions);
    }


    private void printHeader() {
        printLine(STATEMENT_HEADER);
    }


    private void printTransactions(List<Transaction> transactions) {
        Transaction transaction = new Transaction();
        transaction.printTransactions(transactions);
        /*transactions.stream()
                .map(transaction -> statementLine(transaction, balance.addAndGet(transaction.amount())))
                .collect(toCollection(LinkedList::new))
                .descendingIterator()
                .forEachRemaining(this::printLine);*/
    }


    private Transaction transactionWith(int amount) {
        return new Transaction(clock.today(), amount);
    }


    private void printLine(String line) {
        console.printLine(line);
    }
}